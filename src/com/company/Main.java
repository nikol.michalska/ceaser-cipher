package com.company;

import java.util.Scanner;

public class Main {
    static Scanner scanner = new Scanner(System.in);
    static String alphabet = "abcdefghijklmnopqrstuvwxyz";

    public static void main(String[] args) {
        printMenu();
        mainMenu();

    }

    public static void printMenu() {
        System.out.println("Please choose one of the option:  encrypt or decrypt the word?");
        System.out.println("1. Encrypt the code");
        System.out.println("2. Decrypt the code");
    }

    public static void mainMenu() {
        int userChoice = scanner.nextInt();
        switch (userChoice) {
            case 1:
                System.out.println("Choose the number of how many places you want to move the word"); // o ile cyferek chce przesunac
                int numberToMove = scanner.nextInt();
                System.out.println("What word do you want to encode?");
                String wordToEncode = scanner.next();
                encryption(wordToEncode, numberToMove);
                break;
            case 2:
                System.out.println("Choose the number of how many places you want to move the word");
                int numberToMoveDecrypt = scanner.nextInt();
                System.out.println("Put the word to decrypt");
                String wordToDecrypt = scanner.next();
                decryption(wordToDecrypt, numberToMoveDecrypt);

                break;
            default:
                System.out.println("Finish");
        }
    }

    public static String encryption(String wordToEncode, int numberToMove) {
        for (int a = 0; a < wordToEncode.length(); a++) {
            int charPosition = alphabet.indexOf(wordToEncode.charAt(a));
            int valueToMove = (numberToMove + charPosition) % 26;
            char keyValue = alphabet.charAt(valueToMove);
            System.out.print(keyValue);
        }
        return wordToEncode;
    }

    public static String decryption(String wordToDecrypt, int numberToMove) {
        String message = "";

        for (int a = 0; a < wordToDecrypt.length(); a++) {
            int charPosition = alphabet.indexOf(wordToDecrypt.charAt(a));
            int keyValue = (charPosition - numberToMove) % 26;
            if (keyValue < 0) {
                keyValue = alphabet.length() + keyValue;
            }
            char replaceValue = alphabet.charAt(keyValue);
            System.out.print(replaceValue);
        }
        return message;
    }
}

